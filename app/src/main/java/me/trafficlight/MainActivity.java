package me.trafficlight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.1
 * @since 02/17/2021
 */
public class MainActivity extends AppCompatActivity {
    private ConstraintLayout layout;

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layout = findViewById(R.id.layout);
    }

    /**
     * Restores data states from {@code savedInstanceState}.
     *
     * @param savedInstanceState data states for restoring.
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        layout.setBackgroundColor(savedInstanceState.getInt("LAYOUT_BACKGROUND_COLOR_STATE_KEY"));
    }

    /**
     * Saves data states to {@code outState}.
     *
     * @param outState data states for saving.
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("LAYOUT_BACKGROUND_COLOR_STATE_KEY", ((ColorDrawable) layout.getBackground()).getColor());
        super.onSaveInstanceState(outState);
    }

    /**
     * Sets the {@code layout} background color to red.
     *
     * @param view user interface components.
     */
    public void onClickRedButton(View view) {
        layout.setBackgroundResource(R.color.red);
    }

    /**
     * Sets the {@code layout} background color to yellow.
     *
     * @param view user interface components.
     */
    public void onClickYellowButton(View view) {
        layout.setBackgroundResource(R.color.yellow);
    }

    /**
     * Sets the {@code layout} background color to green.
     *
     * @param view user interface components.
     */
    public void onClickGreenButton(View view) {
        layout.setBackgroundResource(R.color.green);
    }
}